package org.ksga.springboot.springmvcdemo.controller.rest;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api")
public class PersonRestController {
    @GetMapping("/person")
    public ResponseEntity<?> person() {
        Map<String, Object> resp = new HashMap<>();
        resp.put("title", "Book 1");
        return ResponseEntity.ok().body(resp);
    }
}
