package org.ksga.springboot.springmvcdemo.controller;

import org.ksga.springboot.springmvcdemo.model.Person;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;

@Controller
public class PersonController {

    String text = "Hello My Text";

    @GetMapping
    public String index(Model model) {
        model.addAttribute("text", "Hello Thisdmflaskdfklsldkfkldsklf");
        model.addAttribute("text2", "Hello");
        model.addAttribute("text", "Hello Thisdmflaskdfklsldkfkldsklf");
        model.addAttribute("text2", "Hello");
        model.addAttribute("text", "Hello Thisdmflaskdfklsldkfkldsklf");
        model.addAttribute("text2", "Hello");

        return "index";
    }

    @GetMapping("/model")
    public ModelAndView modelAndView() {
        ModelAndView m = new ModelAndView();

        Person person = new Person();
        person.setId(1);
        person.setName("KSHRD");

        List<Person> people = new ArrayList<>();
        people.add(person);

        m.setViewName("index");
        m.addObject("people", people);
        return m;
    }

}
